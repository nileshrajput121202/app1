import 'package:flutter/material.dart';
import 'package:flutter_application_1/MapScreen.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        
        // backgroundColor: Colors.purple,
         appBar: AppBar(
          backgroundColor: Colors.deepOrange,
          title: Text("Login Page"),
         ),
         body: Padding(
           
           padding: const EdgeInsets.fromLTRB(25,25,25,0),
           child: Column(
          
            children: [ 
              
              Image.asset('image/project.jpg'),

              Column(
                
                children: [ 
                  SizedBox(height: 45,),
                  SizedBox(width: 5,),
                  TextField( 
                    decoration: InputDecoration( 
                      
                      labelText: "USER NAME"
                    ),
                  ),
                  SizedBox(height: 15,),
                  TextField( 
                     decoration: InputDecoration( 
                      labelText: "PASSWARD"
                     ),
                  ),
                  Padding(
                    
                    padding: const EdgeInsets.fromLTRB(0,20,240,0),
                  
                    child: Column(children: [ 
                        
                        TextButton(onPressed:(){
                    
                        }, child: Text("FORGETPASS",style: TextStyle(fontSize: 12),),),
                    ],),
                  ),
                  Column( 
                    children: [ 
                      SizedBox(height: 25,),
                      ElevatedButton(onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context)=> MapScreen()),
                            );
                       }
                      
                      , child: Text("LOGIN",style: TextStyle(fontSize: 20),))
                    ],
                  ),
                  Column( 
                    
                    children: [ 
                      SizedBox(height: 100,),
                      ElevatedButton(onPressed: (){

                      }, child: Text("SIGN-UP")),
                    ],
                  )
                ],
                
              )
            ],
            
           ),
           
           
         ),
      );
  }
}


